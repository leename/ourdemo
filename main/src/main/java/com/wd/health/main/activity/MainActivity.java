package com.wd.health.main.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.AppCompatImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.wd.health.Constant;
import com.wd.health.base.BaseActivity;
import com.wd.health.main.R;
import com.wd.health.main.fragment.HomeFragment;
import com.wd.health.main.fragment.PatientsFragment;
import com.wd.health.main.fragment.VideoFragment;

@Route(path = Constant.ACTIVITY_URL_MAIN)
public class MainActivity extends BaseActivity {

    private AppCompatImageView mMainPatientsIv;
    private RadioButton mMainRgHome, mMainRgVideo, mMainRgPatients;
    private RadioGroup mMainRg;
    private HomeFragment mHomeFragment;
    private PatientsFragment mPatientsFragment;
    private VideoFragment mVideoFragment;
    private Fragment mFragment;//当前显示的Fragment

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.main_fl, mHomeFragment).commit();
        mFragment = mHomeFragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void initData() {
        /*
         *  从14开始，library中的资源id就不是final类型的了
         *  所以 这里只能使用if else
         */
        mMainRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int i) {
                if (i == R.id.main_rg_home) {

                    mMainRgHome.setChecked(true);
                    mMainRgVideo.setChecked(false);
                    mMainRgPatients.setChecked(false);
                    switchFragment(mHomeFragment);

                } else if (i == R.id.main_rg_patients) {

                    mMainRgHome.setChecked(false);
                    mMainRgVideo.setChecked(false);
                    mMainRgPatients.setChecked(true);
                    switchFragment(mPatientsFragment);

                } else if (i == R.id.main_rg_video) {

                    mMainRgHome.setChecked(false);
                    mMainRgVideo.setChecked(true);
                    mMainRgPatients.setChecked(false);
                    switchFragment(mVideoFragment);

                }
            }
        });
    }

    private void initFragment() {
        mHomeFragment = new HomeFragment();
        mPatientsFragment = new PatientsFragment();
        mVideoFragment = new VideoFragment();
    }

    @Override
    protected void initView() {
        mMainPatientsIv = (AppCompatImageView) findViewById(R.id.main_patients_iv);
        mMainRgHome = (RadioButton) findViewById(R.id.main_rg_home);
        mMainRgVideo = (RadioButton) findViewById(R.id.main_rg_video);
        mMainRg = (RadioGroup) findViewById(R.id.main_rg);
        mMainRgPatients = (RadioButton) findViewById(R.id.main_rg_patients);
        initFragment();
    }

    @Override
    protected void destroyData() {

    }

    private void switchFragment(Fragment fragment) {
        //判断当前显示的Fragment是不是切换的Fragment
        if (mFragment != fragment) {
            //判断切换的Fragment是否已经添加过
            if (!fragment.isAdded()) {
                //如果没有，则先把当前的Fragment隐藏，把切换的Fragment添加上
                getSupportFragmentManager().beginTransaction().hide(mFragment)
                        .add(R.id.main_fl, fragment).commit();
            } else {
                //如果已经添加过，则先把当前的Fragment隐藏，把切换的Fragment显示出来
                getSupportFragmentManager().beginTransaction().hide(mFragment).show(fragment).commit();
            }
            mFragment = fragment;
        }
    }

}
