package com.wd.health.main.fragment;

import android.view.View;

import com.wd.health.base.BaseFragment;
import com.wd.health.main.R;

/**
 * Created :  LiZhIX
 * Date :  2019/8/5 14:56
 * Description  :  Fragment 首页
 */
public class HomeFragment extends BaseFragment {

    @Override
    protected void setViewData(View view) {

    }

    @Override
    protected void initView(View view) {

    }

    @Override
    public int setLayoutView() {
        return R.layout.fragment_home;
    }


}
