package com.wd.health.login;

import android.os.Bundle;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.wd.health.Constant;
import com.wd.health.base.BaseActivity;

@Route(path = Constant.ACTIVITY_URL_LOGIN)
public class LoginActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_login;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void destroyData() {

    }

//        mTextView = (TextView) findViewById(R.id.textView);
//        mTextView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ARouter.getInstance().build(Constant.ACTIVITY_URL_MAIN)
//                        .navigation();
//            }
//        });

}

