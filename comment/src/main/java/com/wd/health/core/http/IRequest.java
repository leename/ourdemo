package com.wd.health.core.http;

import com.wd.health.model.Result;
import com.wd.health.model.bean.UserInfoBean;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created :  LiZhIX
 * Date :  2019/8/2 23:11
 * Description  :
 */
public interface IRequest {

    @FormUrlEncoded
    @POST(ApiService.LOGIN_URL)
    Observable<Result<UserInfoBean>> login(@Field("phone") String phone,
                                           @Field("pwd") String pwd);


}